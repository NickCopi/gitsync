# Gitsync 
 
Syncs repos from gitlab to github. 

## Usage 
Configure a .env file with the following values
```
GITLAB_TOKEN=<Gitlab personal access token>
GITHUB_TOKEN=<Github personal access token>
GITLAB_ID=<Gitlab account ID number>
GITHUB_USERNAME=<Github username>
GITLAB_USERNAME=<Gitlab username>
```
And run `npm install` and `node index.js` in a writable folder.
